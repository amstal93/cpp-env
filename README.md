# C++ Environment

Clang-based container for building C++ applications.

```sh
docker pull registry.gitlab.com/signal9/cpp-env:13.0.0
```

## Features

* CMake
* Clang
* Conan
* LLD linker
* LLVM
* Ninja
* OpenMP
* Polly
* compiler-rt
* libc++
* libunwind
